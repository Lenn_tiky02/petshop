<?php

/**
 *
 * @author davra
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class MY_Model extends CI_Model {

    public function create($options_echappees = array(), $options_non_echappees = array()) {
        if (empty($options_echappees) AND empty($options_non_echappees) ) {
            return false;
        }
        return (bool) $this->db->set($options_echappees)
                        ->set($options_non_echappees, null, false)
                        ->insert($this->table);
    }

    //valeur par défaut = *
    //where tableau
    public function read($select = '*', $where = array(), $nb = null, $debut = null) {
        return $this->db->select($select)
                        ->from($this->table)
                        ->where($where)
                        ->limit($nb, $debut)
                        ->get()
                        ->result();
    }
    //where String
    public function readStringWhere($select = '*', $where, $nb = null, $debut = null) {
        return $this->db->select($select)
                        ->from($this->table)
                        ->where($where)
                        ->limit($nb, $debut)
                        ->get()
                        ->result();
    }

    public function update($where, $options_echappees = array(), $options_non_echappees = array()) {
        if (empty($options_echappees) AND empty($options_non_echappees)) {
            return false;
        }
        if (is_integer($where)) {
            $where = array('id' => $where);
        }
        return (bool) $this->db->set($options_echappees)
                        ->set($options_non_echappees, null, false)
                        ->where($where)
                        ->update($this->table);
    }

    public function delete($where) {
        if (is_integer($where)) {
            $where = array('id' => $where);
        }
        return (bool) $this->db->where($where)
                        ->delete($this->table);
    }

     public function count($champ = array(), $valeur = null) 
    {
        return (int) $this->db->where($champ, $valeur)
                            ->from($this->table)
                            ->count_all_results();
    }

    public function getIdInsert($predicat){
        $str="concat('". $predicat ."', nextval('". $predicat ."'))";       
        return $this->db->select($str)                                              
                        ->get()
                        ->result();
    }

}
