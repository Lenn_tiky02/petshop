<?php
    class Utilisateur_model extends MY_Model{
        protected $table='utilisateur';
        protected $predicat='usr';

        private $idUtilisateur ;
        private $nom;
        private $prenoms;
        private $email;
        private $motdepasse;
        private $permission;     

        public function __construct() {

            parent::__construct();
            
         }

        public function initInsert($idUtilisateur,$nom,$prenoms,$email,$motdepasse,$permission){
            //identification
            $this->setId($idUtilisateur);
            $this->setNom($nom);
            $this->setPrenoms($prenoms);
            $this->setEmail($email);
            $this->setMotdepasse($motdepasse);
            $this->setPermission($permission);
        }

        public function getDonneesEchappees(){

            $data['idUtilisateur']=$this->getId();
            $data['nom']=$this->getNom();
            $data['prenoms']=$this->getPrenoms();
            $data['email']=$this->getEmail();
            $data['motdepasse']=$this->getMotdepasse();       
            $data['permission']=$this->getPermission();
            
            return $data;
        }

        public function getDonneesNonEchappees(){         
            return $data;
        }        


        public function getId(){
            return  $this->idUtilisateur;
        }
        public function setId($idUtilisateur){
            $this->idUtilisateur=$idUtilisateur;
        }
        public function getNom(){
            return  $this->nom;
        }
        public function setNom($nom){
            $this->nom=$nom;
        }
        public function getPrenoms(){
            return  $this->prenoms;
        }
        public function setPrenoms($prenoms){
            $this->prenoms=$prenoms;
        } 
        public function getEmail(){
            return  $this->email;
        }
        public function setEmail($email){
            $this->email=$email;
        } 
        public function getMotdepasse(){
            return  $this->motdepasse;
        }
        public function setMotdepasse($motdepasse){
            $this->motdepasse=$motdepasse;
        }
        public function getPermission(){
            return  $this->permission;
        }
        public function setPermission($permission){
            $this->permission=$permission;
        }
}
?>