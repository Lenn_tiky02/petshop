<?php
    class Animaux_model extends MY_Model{
        protected $table='animaux';
        protected $predicat='anm';

        private $idanimaux ;
        private $idcategorie;
        private $idrace;
        private $nom;        
        private $poids;
        private $taille;
        private $sexe;
        private $description;        
        private $naissance;
        private $prix;
        private $titreimage;
        private $referenceimage;
        private $descriptionimage;
        private $disponibilite;

        public function __construct() {

            parent::__construct();
            
         }

        public function initInsert($id,$idcategorie,$idrace,$nom,$poids,$taille,$sexe,$description,$naissance,$prix,
        $titreimage,$referenceimage,$descriptionimage,$disponibilite){
            $this->setId($id);
            $this->setidcategorie($idcategorie);
            $this->setidrace($idrace);
            $this->setNom($nom);
            $this->setPoids($poids);
            $this->setTaille($taille);
            $this->setSexe($sexe);
            $this->setDescription($description);
            $this->setNaissance($naissance);
            $this->setPrix($prix);
            $this->setTitreimage($titreimage);
            $this->setReferenceimage($referenceimage);
            $this->setDescriptionimage($descriptionimage);
            $this->setDisponibilite($disponibilite);
           
        }

        public function getDonneesEchappees(){

            $data['idanimaux']=$this->getId();
            $data['idcategorie']=$this->getidcategorie();
            $data['idrace']=$this->getidrace();
            $data['nom']=$this->getNom();
            $data['poids']=$this->getPoids();
            $data['taille']=$this->getTaille();
            $data['sexe']=$this->getSexe();
            $data['description']=$this->getDescription();            
            $data['naissance']=$this->getNaissance();
            $data['prix']=$this->getPrix();
            $data['titreimage']=$this->getTitreimage();            
            $data['referenceimage']=$this->getReferenceimage();            
            $data['descriptionimage']=$this->getDescriptionimage();
            $data['disponibilite']=$this->getDisponibilite();
            
            return $data;
        }

        public function getDonneesNonEchappees(){         
            return $data;
        }        

        
        public function getTable(){
            return  $this->table;
        }
        public function setTable($table){
            $this->table=$table;
        }        
        public function getPredicat(){
            return  $this->predicat;
        }
        public function setPredicat($predicat){
            $this->predicat=$predicat;
        }



        public function getId(){
            return  $this->idanimaux;
        }
        public function setId($idanimaux){
            $this->idanimaux=$idanimaux;
        }
        public function getidcategorie(){
            return  $this->idcategorie;
        }
        public function setidcategorie($idcategorie){
            $this->idcategorie=$idcategorie;
        }
        public function getidrace(){
            return  $this->idrace;
        }
        public function setidrace($idrace){
            $this->idrace=$idrace;
        }
        public function getNom(){
            return  $this->nom;
        }
        public function setNom($nom){
            $this->nom=$nom;
        }
        public function getPoids(){
            return  $this->poids;
        }
        public function setPoids($poids){
            $this->poids=$poids;
        } 
        public function getTaille(){
            return  $this->taille;
        }
        public function setTaille($taille){
            $this->taille=$taille;
        } 
        public function getSexe(){
            return  $this->sexe;
        }
        public function setSexe($sexe){
            $this->sexe=$sexe;
        }
        public function getDescription(){
            return  $this->description;
        }
        public function setDescription($description){
            $this->description=$description;
        }
        public function getNaissance(){
            return  $this->naissance;
        }
        public function setNaissance($naissance){
            $this->naissance=$naissance;
        }
        public function getPrix(){
            return  $this->dateFin;
        }
        public function setPrix($dateFin){
            $this->dateFin=$dateFin;
        }    
        public function getTitreimage(){
            return  $this->titreimage;
        }
        public function setTitreimage($titreimage){
            $this->titreimage=$titreimage;
        }
        public function getReferenceimage(){
            return  $this->referenceimage;
        }
        public function setReferenceimage($referenceimage){
            $this->referenceimage=$referenceimage;
        }         
        public function getDescriptionimage(){
            return  $this->descriptionimage;
        }
        public function setDescriptionimage($descriptionimage){
            $this->descriptionimage=$descriptionimage;
        } 
        public function getDisponibilite(){
            return  $this->disponibilite;
        }
        public function setDisponibilite($disponibilite){
            $this->disponibilite=$disponibilite;
        }
    
}
?>