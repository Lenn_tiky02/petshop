<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="<?php echo base_url("assets/img/favicon.ico"); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Light Bootstrap Dashboard by Creative Tim</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url("assets/css/animate.min.css"); ?>" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="<?php echo base_url("assets/css/light-bootstrap-dashboard.css?v=1.4.0"); ?>" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url("assets/css/demo.css"); ?>" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url("assets/css/pe-icon-7-stroke.css"); ?>" rel="stylesheet" />
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="green" data-image="<?php echo base_url("assets/img/sidebar-6.jpg"); ?>">

    <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->

<!-- navigation -->
    	<div class="sidebar-wrapper">
            <div class="logo">
                <img src="<?php echo base_url("assets/img/logo.png"); ?>" alt="Logo" />
            </div>

            <ul class="nav">
            <li class="active">
                <a href="<?php echo base_url("home"); ?>">
                    <i class="pe-7s-note2"></i>
                    <p>Table List</p>
                </a>
            </li>
            <li class="active">
            <a href="<?php echo base_url("addpet"); ?>">
                <i class="pe-7s-box2"></i>
                <p>Add a pet</p>
            </a>
            </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
		<nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Pets List</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                       
                        <li>
                            <a href="<?php echo base_url("home/deconnexion"); ?>">
                            <p><i class="pe-7s-power"></i> &nbsp; Log out</p>
                            </a>
                        </li>
						<li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>

<!-- body -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="header">
                                <h4 class="title">Add Pet in the List of the pet in the Front Office WebSite:</h4>
                                <p class="category">Complete the form:</p>
                            </div>

<!--                            formulaire -->
                            <div class="content">
                            <form  action="<?php echo base_url("addpet/insertionanimaux");?>" method="post">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Category:</label>
                                            <select class="form-control" name="idcategorie" id="idcategorie">
                                            <option value="cat1">cat1</option>
                                            </select>                                            
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Race:</label>
                                            <select class="form-control"name="idrace" id="idrace">
                                            <option value="rac1">rac1</option>
                                            </select>
                                        </div>
                                    </div>
                                  
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label >Name:</label>
                                            <input type="text" class="form-control" placeholder="name" name="nom" id="nom">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                   <div class="col-md-4">
                                        <div class="form-group">
                                            <label >Weight:</label>
                                            <input type="number" class="form-control" placeholder="kg" name="poids" id="poids">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label >Height:</label>
                                            <input type="number" class="form-control" placeholder="cm" name="taille" id="taille">
                                        </div>
                                    </div>                                
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Gender:</label>
                                            <select class="form-control" name="sexe" id="sexe">
                                                <option value="M">Male</option>
                                                <option value="F">Female</option>
                                            </select>                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Date of birth:</label>
                                            <input type="date" class="form-control" name="naissance" id="naissance">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Price:</label>
                                            <input type="number" class="form-control" name="prix" id="prix">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label >Title of the picture:</label>
                                            <input type="text" class="form-control" name="titreimage" id="titreimage">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label >Name of the picture:</label>
                                            <input type="text" class="form-control" name="referenceimage" id="referenceimage">
                                        </div>
                                    </div> 
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label >Description of the picture:</label>
                                            <input type="text" class="form-control" name="descriptionimage" id="descriptionimage">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label >Upload of the picture:</label>
                                            <input type="file" class="form-control" name="cheminimage" id="cheminimage">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea rows="5" class="form-control" placeholder="Here can be your description" value="Mike" name="description" id="description"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-info btn-fill pull-right">Insert</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>





                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="#">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Company
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Portfolio
                            </a>
                        </li>
                        <li>
                            <a href="#">
                               Blog
                            </a>
                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                </p>
            </div>
        </footer>


    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="<?php echo base_url("assets/js/jquery.3.2.1.min.js"); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url("assets/js/bootstrap.min.js"); ?>" type="text/javascript"></script>

	<!--  Charts Plugin -->
	<script src="<?php echo base_url("assets/js/chartist.min.js"); ?>"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url("assets/js/bootstrap-notify.js"); ?>"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="<?php echo base_url("assets/js/light-bootstrap-dashboard.js?v=1.4.0"); ?>"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="<?php echo base_url("assets/js/demo.js"); ?>"></script>


</html>
