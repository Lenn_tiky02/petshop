<?php
defined('BASEPATH') OR exit('No direct script access allowed');
session_start();
class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		if($_SESSION['user']=='ok'){
			$tab['pet_tab']=$this->selectPets(0);
			$this->load->view('home_view',$tab);				
		}else{
			redirect('');
		}
	}
	private function selectPets($debut){
		$this->load->model('animaux_model');
		return $this->animaux_model->read('*',array(),10,$debut);
		
	}
	public function verrification()
	{
		$test=false;
		$this->load->model('utilisateur_model','user');
		$tab=$this->user->read('*',array(),null,null);
				
		foreach($tab as $detail){
			if($detail->email==$_POST['username'] && $detail->motdepasse==$_POST['password']){
				$test=true;
				
			}
		}
		if($test){
			$_SESSION['user']='ok';	
			$tab['pet_tab']=$this->selectPets(0);
			$this->load->view('home_view',$tab);				
		}else{
			redirect('');
		}
	}		
	public function deconnexion()
	{	
		session_destroy();
		redirect('');
	}		
}
?>