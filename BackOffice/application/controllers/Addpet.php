<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Addpet extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('addpet_view');		
	}
		
	public function insertionanimaux()
	{
		 
		
        
        $idcategorie =$_POST['idcategorie'];
        $idrace = $_POST['idrace'];
        $nom = $_POST['nom'] ;        
        $poids = $_POST['poids'] ;
        $taille = $_POST['taille'];
        $sexe = $_POST['sexe'];
        $description = 	$_POST['description'];        
        $naissance = $_POST['naissance'] ;
        $prix = $_POST['prix'];
        $titreimage = $_POST['titreimage'];
        $referenceimage = $_POST['referenceimage'];
        $descriptionimage = $_POST['descriptionimage'];
				
		$this->load->model('animaux_model','animaux');
		$idanimaux = $this->animaux->getIdInsert($this->animaux->getPredicat())[0]->concat;
		$this->animaux->initInsert($idanimaux,$idcategorie,$idrace,$nom,$poids,$taille,$sexe,$description,$naissance,$prix,
		$titreimage,$referenceimage,$descriptionimage,1);
		$tab=$this->animaux->getDonneesEchappees();
		$booleen=$this->animaux->create(array(),$tab);
		var_dump($tab);
		echo "Insertion =".$booleen;
		//redirect('home');
	}	
}
?>