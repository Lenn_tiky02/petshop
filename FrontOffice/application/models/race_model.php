<?php
    class Race_model extends MY_Model{
        protected $table='race';
        protected $predicat='rac';

        private $idRace;
        private $idCategorie;
        private $nom;   

        public function __construct() {

            parent::__construct();
            
         }

        public function initInsert($idRace,$idCategorie,$nom){

            $this->setIdRace($idRace);
            $this->setIdCategorie($idCategorie);           
            $this->setNom($nom);
           
        }

        public function getDonneesEchappees(){

            $data['idRace']=$this->getIdRace();
            $data['idCategorie']=$this->getIdCategorie();
            $data['nom']=$this->getNom();
            return $data;
        }

        public function getDonneesNonEchappees(){         
            return $data;
        }        

        public function getIdRace(){
            return  $this->idRace;
        }
        public function setIdRace($idRace){
            $this->idRace=$idRace;
        }
        public function getIdCategorie(){
            return  $this->idCategorie;
        }
        public function setIdCategorie($idCategorie){
            $this->idCategorie=$idCategorie;
        }
        public function getNom(){
            return  $this->nom;
        }
        public function setNom($nom){
            $this->nom=$nom;
        }
}
?>