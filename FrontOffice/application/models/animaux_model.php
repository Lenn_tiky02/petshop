<?php
    class Animaux_model extends MY_Model{
        protected $table='animaux';
        protected $predicat='anm';

        private $idCategorie;
        private $idRace;
        private $nom;        
        private $poids;
        private $taille;
        private $sexe;
        private $description;        
        private $naissance;
        private $prix;
        private $titreimage;
        private $referenceimage;
        private $descriptionimage;

        public function __construct() {

            parent::__construct();
            
         }

        public function initInsert($idCategorie,$idRace,$nom,$description,$prix,$titreimage,$referenceimage,
        $descriptionimage){
            //identification
            $this->setIdCategorie($idCategorie);
            $this->setIdRace($idRace);
            $this->setNom($nom);
            $this->setDescription($description);
            $this->setPrix($prix);
            $this->setTitreimage($titreimage);
            $this->setReferenceimage($referenceimage);
            $this->setDescriptionimage($descriptionimage);
           
        }

        public function getDonneesEchappees(){

            $data['idAnimaux']=$this->getId();
            $data['idCategorie']=$this->getIdCategorie();
            $data['idRace']=$this->getIdRace();
            $data['nom']=$this->getNom();
            $data['poids']=$this->getPoids();
            $data['taille']=$this->getTaille();
            $data['sexe']=$this->getSexe();
            $data['description']=$this->getDescription();            
            $data['naissance']=$this->getNaissance();
            $data['prix']=$this->getPrix();
            $data['titreimage']=$this->getTitreimage();            
            $data['referenceimage']=$this->getReferenceimage();            
            $data['descriptionimage']=$this->getDescriptionimage();
            
            return $data;
        }

        public function getDonneesNonEchappees(){         
            return $data;
        }        


        public function getId(){
            return  $this->idAnimaux;
        }
        public function setId($idAnimaux){
            $this->idAnimaux=$idAnimaux;
        }
        public function getIdCategorie(){
            return  $this->idCategorie;
        }
        public function setIdCategorie($idCategorie){
            $this->idCategorie=$idCategorie;
        }
        public function getIdRace(){
            return  $this->idRace;
        }
        public function setIdRace($idRace){
            $this->idRace=$idRace;
        }
        public function getNom(){
            return  $this->nom;
        }
        public function setNom($nom){
            $this->nom=$nom;
        }
        public function getPoids(){
            return  $this->poids;
        }
        public function setPoids($poids){
            $this->poids=$poids;
        } 
        public function getTaille(){
            return  $this->taille;
        }
        public function setTaille($taille){
            $this->taille=$taille;
        } 
        public function getSexe(){
            return  $this->sexe;
        }
        public function setSexe($sexe){
            $this->sexe=$sexe;
        }
        public function getDescription(){
            return  $this->description;
        }
        public function setDescription($description){
            $this->description=$description;
        }
        public function getNaissance(){
            return  $this->naissance;
        }
        public function setNaissance($naissance){
            $this->naissance=$naissance;
        }
        public function getPrix(){
            return  $this->dateFin;
        }
        public function setPrix($dateFin){
            $this->dateFin=$dateFin;
        }    
        public function getTitreimage(){
            return  $this->titreimage;
        }
        public function setTitreimage($titreimage){
            $this->titreimage=$titreimage;
        }
        public function getReferenceimage(){
            return  $this->referenceimage;
        }
        public function setReferenceimage($referenceimage){
            $this->referenceimage=$referenceimage;
        }         
        public function getDescriptionimage(){
            return  $this->descriptionimage;
        }
        public function setDescriptionimage($descriptionimage){
            $this->descriptionimage=$descriptionimage;
        }
    
}
?>
