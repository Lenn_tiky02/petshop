<?php
    class Categorie_model extends MY_Model{
        protected $table='categorie';
        protected $predicat='cat';

        private $id;      
        private $nom;   

        public function __construct() {

            parent::__construct();
            
         }

        public function initInsert($id,$nom){
            $this->setId($id);
            $this->setNom($nom);           
        }

        public function getDonneesEchappees(){

            $data['id']=$this->getId();           
            $data['nom']=$this->getNom();
            return $data;
        }

        public function getDonneesNonEchappees(){         
            return $data;
        }        


        public function getId(){
            return  $this->id;
        }
        public function setId($id){
            $this->id=$id;
        }
        public function getNom(){
            return  $this->nom;
        }
        public function setNom($nom){
            $this->nom=$nom;
        }
}
?>