<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pets extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('animaux_model');
		$tab["animaux_data"]=$this->animaux_model->read('*',array(),10,0);
		$this->load->view('pets_view',$tab);		
	}
	public function petDetail($id)
	{
		$this->load->model('animaux_model');
		$where=array();
		$where["idanimaux"]=$id;
		$tab["animaux_data"]=$this->animaux_model->read('*',$where,10,0);
		
		$this->load->view('petdetail_view',$tab);
	}


}
?>