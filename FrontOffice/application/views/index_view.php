<!DOCTYPE html>
<html>
<head>
<title>Pet-Shop | Trouvez votre animal de compianie d'amour</title>
<meta charset="iso-8859-1"name="keywords" content="Trouvez l'animal de vos rêve ici. N'hésitez pas à choisir et contactez-nous vite pour réserver votre animal de companie. ">
<link href="<?php echo base_url("assets/css/style.css"); ?>" rel="stylesheet" type="text/css">

<!--[if IE 6]><link href="css/ie6.css" rel="stylesheet" type="text/css"><![endif]-->
<!--[if IE 7]><link href="css/ie7.css" rel="stylesheet" type="text/css"><![endif]-->
</head>
<body>
<div id="header"> <a href="#" id="logo"><img src="<?php echo base_url("assets/images/logo.gif");?>" width="310" height="114" alt=""></a>
  <ul class="navigation">
      <li class="active"><a href="<?php echo site_url("home");?>">Accueil</a></li>
      <li><a href="<?php echo site_url("pets");?>">Animaux</a></li>
      <li><a href="<?php echo site_url("contact");?>">Contacts</a></li>
  </ul>
</div>

<div id="body">
  <div class="banner">&nbsp;</div>

  <div id="content">

    <div class="content">
      <ul>
      <li> <a href="#"><img src="<?php echo base_url("assets/images/puppy.jpg");?>" width="114" height="160" alt=""></a>
          <h2>Adopter un chien?</h2>
          <p>Un <strong>chien </strong>peut enrichir votre vie à bien des égards. Dit le meilleur ami de l'homme.  Les chiens améliorent notre moral et notre sentiment de bien-être. Un bon système de sécurité.</p>
        </li>
        <li> <a href="#"><img src="<?php echo base_url("assets/images/cat.jpg");?>" width="114" height="160" alt=""></a>
          <h2>Adopter un chat?</h2>
          <p>Les <strong>chats</strong> sont réputés pour être des animaux mystérieux et indépendants.Qui n'a jamais craqué devant la bouille d'un chat qui ronronne ? En plus il sait très bien vous consoler quand vous êtes triste!</p>
        </li>
        <li> <a href="#"><img src="<?php echo base_url("assets/images/koi.jpg");?>" width="114" height="160" alt=""></a>
          <h2>Adopter un poisson?</h2>
          <p>Adopter un animal de compagnie c’est bien, s’en occuper, c’est mieux ! Vous souhaitez avoir de la compagnie quand vous rentrez chez vous mais vous n’avez pas le temps ou la place d’accueillir un chien ou un chat ? Les jolies petits <strong>poissons </strong>sont faites pour vous !</p>
        </li>
        <li> <a href="#"><img src="<?php echo base_url("assets/images/bird.jpg");?>" width="114" height="160" alt=""></a>
          <h2>Adopter un oiseau?</h2>
          <p>L’assurance d’adopter un <strong>oiseau</strong> en bonne santé .Vous voulez absolument une espèce en particulier ?Nous allons vous donner le meilleur conseil. Vous économiserez de l’argent pour offrir un meilleur confort de vie à votre oiseau.</p>
        </li> 
      </ul>
    </div>
<!-- debut sidebar-->
    <div id="sidebar">

      <div class="search">
        <input type="text" name="s" value="Find">
        <button>&nbsp;</button>
        <label for="articles">
          <input type="radio" id="articles">
          Articles</label>
        <label for="products">
          <input type="radio" id="products" checked>
          PetMart Products</label>
      </div>

      <div class="section">
        <ul class="navigation">
          <li class="active"><a href="#">Shopping Guides</a></li>
          <li><a href="#">Discounted Items</a></li>
        </ul>
        <div class="aside">
          <div>
            <div>
              <ul>
                <li><a href="#">Pet Accesories </a> <a class="more" href="#">see all</a></li>
                <li><a href="#">Bath Essentials</a> <a class="more" href="#">see all</a></li>
                <li><a href="#">Pet Food</a> <a class="more" href="#">see all</a></li>
                <li><a href="#">Pet Vitamins</a> <a class="more" href="#">see all</a></li>
                <li><a href="#">Pet Needs</a> <a class="more" href="#">see all</a></li>
                <li><a href="#">Pet Toy and Treats</a> <a class="more" href="#">see all</a></li>
                <li><a href="#">Pet Supplies</a> <a class="more" href="#">see all</a></li>
                <li><a href="#">Pet Emergency Kit</a> <a class="more" href="#">see all</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

    </div><!-- fin sidebar -->
  </div>


  <div class="featured">
    <ul>
      <li><a href="#"><img src="<?php echo base_url("assets/images/organic-and-chemical-free.jpg");?>" width="300" height="90" alt=""></a></li>
      <li><a href="#"><img src="<?php echo base_url("assets/images/good-food.jpg");?>" width="300" height="90" alt=""></a></li>
      <li class="last"><a href="#"><img src="<?php echo base_url("assets/images/pet-grooming.jpg");?>" width="300" height="90" alt=""></a></li>
    </ul>
  </div>

</div>


<div id="footer">
  <div class="section" style="font-color: color.red;">
    <ul>
      <li> <img src="<?php echo base_url("assets/images/friendly-pets.jpg");?>" width="240" height="186" alt="">
        Friendly Pets
      </li>
      <li> <img src="<?php echo base_url("assets/images/pet-lover2.jpg");?>" width="240" height="186" alt="">        
      </li>
      <li> <img src="<?php echo base_url("assets/images/healthy-dog.jpg");?>" width="240" height="186" alt="">
      </li>
      <li>
        <img src="<?php echo base_url("assets/images/pet-lover.jpg");?>" width="240" height="186" alt=""> </li>
    </ul>
  </div>
  <div id="footnote">
    <div class="section">Copyright &copy; 2012 <a href="#">Company Name</a> All rights reserved | Website Template By <a target="_blank" href="http://www.freewebsitetemplates.com/">freewebsitetemplates.com</a></div>
  </div>
</div>
</body>
</html>
