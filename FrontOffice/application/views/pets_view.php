
<!DOCTYPE html>
<html>
<head>
<title>Pet-Shop | Trouvez votre animal de compianie d'amour</title>
<meta charset="iso-8859-1" name="keywords" content="Trouvez l'animal de vos rêve ici. N'hésitez pas à choisir et contactez-nous vite pour réserver votre animal de companie.">
<link href="<?php echo base_url("assets/css/style.css"); ?>" rel="stylesheet" type="text/css">
<!--[if IE 6]><link href="css/ie6.css" rel="stylesheet" type="text/css"><![endif]-->
<!--[if IE 7]><link href="css/ie7.css" rel="stylesheet" type="text/css"><![endif]-->
<!-- e-shopper -->
<!--
<link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/prettyPhoto.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/price-range.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/animate.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/main.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/responsive.css"); ?>" rel="stylesheet">
   -->
</head>
<body>

<div id="header"> <a href="#" id="logo"><img src="<?php echo base_url("assets/images/logo.gif"); ?>" width="310" height="114" alt=""></a>
  <ul class="navigation">
      <li class="active"><a href="<?php echo site_url("home");?>">Accueil</a></li>
      <li><a href="<?php echo site_url("pets");?>">Animaux</a></li>
      <li><a href="<?php echo site_url("contact");?>">Contacts</a></li>
  </ul>
</div>
<div id="body">

  <div id="content">
    <div class="content">
      <h2>Liste des animaux</h2>
      <div>
        <p>Trouvez l'<strong>animal</strong> de vos rêve ici. N'hésitez pas à choisir et contactez-nous vite pour réserver votre <strong>animal de companie</strong>. Cliquez sur contacts en haut pour nous contacter</p>
      </div>

      <div>
      <ul class="section">
      <?php
         foreach($animaux_data as $detail){
          echo " <li>";
          echo "<h2>".$detail->descriptionimage."</h2>";
          echo "<a href='".base_url("Pets/".$detail->nom."-".$detail->descriptionimage."-".$detail->idrace."-".$detail->idanimaux.".html")."'><img src='". base_url("assets". $detail->referenceimage) ."' width='240' height='175' alt='".$detail->descriptionimage." '></a>";
          //echo "<h3>".$detail->nom."</h3></br>   ";    
          echo "<p>".substr($detail->description,0,50)." ... &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<a class= 'more' href='". base_url("Pets/".$detail->nom."-".$detail->descriptionimage."-".$detail->idrace."-".$detail->idanimaux.".html")."'>(".$detail->nom.")</a></p>";
          echo "</li>";
        }
      ?>  
        </ul> 
      </div>
     
      <div class="paging"> <a class="active" href="#">1</a> <a href="#">2</a> <a href="#">3</a> <a href="#">4</a> <a class="next" href="#">NEXT</a> </div>
      </div>
   <div id="sidebar">
      <div id="section">
        <div>
          <div>
            <h2>Pet Guide Overview</h2>
            <ul>
              <li><a href="#">Pet Trainging Guides <span>(1)</span> </a></li>
              <li><a href="#">Behavior Training <span>(11)</span> </a></li>
              <li><a href="#">Pet Recipes <span>(3)</span> </a></li>
              <li><a href="#">Do's and Don'ts <span>(8)</span> </a></li>
              <li><a href="#">Pet Foods <span>(3)</span> </a></li>
              <li><a href="#">Cosplay Pets <span>(2)</span> </a></li>
              <li><a href="#">Shopping Guides <span></span> </a></li>
              <li><a href="#">Pregnancy and Nursing Pets <span>(8)</span> </a></li>
              <li><a href="#">Medications <span>(7)</span> </a></li>
              <li><a href="#">Excercise <span>(7)</span> </a></li>
              <li><a href="#">Diet <span>(10)</span> </a></li>
              <li><a href="#">Grooming <span>(1)</span> </a></li>
              <li><a href="#">Cosplay Pets <span>(2)</span> </a></li>
              <li><a href="#">Shopping Guides <span></span> </a></li>
              <li><a href="#">Pregnancy and Nursing Pets <span>(8)</span> </a></li>
              <li><a href="#">Medications <span>(7)</span> </a></li>
              <li><a href="#">Excercise <span>(7)</span> </a></li>
              <li><a href="#">Diet <span>(10)</span> </a></li>
              <li><a href="#">Grooming <span>(1)</span> </a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="featured">
    <ul>
      <li><a href="#"><img src="<?php echo base_url("assets/images/organic-and-chemical-free.jpg"); ?>" width="300" height="90" alt=""></a></li>
      <li><a href="#"><img src="<?php echo base_url("assets/images/good-food.jpg"); ?>" width="300" height="90" alt=""></a></li>
      <li class="last"><a href="#"><img src="<?php echo base_url("assets/images/pet-grooming.jpg"); ?>" width="300" height="90" alt=""></a></li>
    </ul>
  </div>
</div>
<div id="footer">
  <div class="section">
    <ul>
      <li> <img src="<?php echo base_url("assets/images/friendly-pets.jpg"); ?>" width="240" height="186" alt="">
      </li>
      <li> <img src="<?php echo base_url("assets/images/pet-lover2.jpg"); ?>" width="240" height="186" alt="">
      </li>
      <li> <img src="<?php echo base_url("assets/images/healthy-dog.jpg"); ?>" width="240" height="186" alt="">
      </li>
      <li>
        <img src="<?php echo base_url("assets/images/pet-lover.jpg"); ?>" width="240" height="186" alt=""> </li>
    </ul>
  </div>
  <div id="footnote">
    <div class="section">Copyright &copy; 2012 <a href="#">Company Name</a> All rights reserved | Website Template By <a target="_blank" href="http://www.freewebsitetemplates.com/">freewebsitetemplates.com</a></div>
  </div>
</div>
<!--
 
-->
</body>
</html>
