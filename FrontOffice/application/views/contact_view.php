<!DOCTYPE html>
<html>
<head>
<title>Pet Shop</title>
<meta charset="iso-8859-1">
<link href="<?php echo base_url("assets/css/style.css"); ?>" rel="stylesheet" type="text/css">

<!--[if IE 6]><link href="css/ie6.css" rel="stylesheet" type="text/css"><![endif]-->
<!--[if IE 7]><link href="css/ie7.css" rel="stylesheet" type="text/css"><![endif]-->
</head>
<body>
<div id="header"> <a href="#" id="logo"><img src="<?php echo base_url("assets/images/logo.gif");?>" width="310" height="114" alt=""></a>
  <ul class="navigation">
    <li class="active"><a href="<?php echo site_url("home");?>">Accueil</a></li>
    <li><a href="<?php echo site_url("pets");?>">Animaux</a></li>
    <li><a href="<?php echo site_url("contact");?>">Contacts</a></li>
  </ul>
</div>

<div id="body">
  <div id="content">
    <div class="content">
      <h2>Contactez-nous vite:</h2>
      <div>
        <p><p>Trouvez l'<strong>animal</strong> de vos rêve ici. N'hésitez pas à choisir et contactez-nous vite pour réserver votre <strong>animal de companie</strong>. </p>
      </div>
      <ul class="connect">
        <li>
          <h2>Adresse e-mail et Téléphone:</h2>
          <p> <span>E-mail: <a href="#">tsikyandrianaivosu@gmail.com</a></span> <span>Appelez ou e-mail nous</span> </p>
          <p> <span>Téléphone: 877 000 0000</span> <span>Téléphone: 866 000 0000</span> <span>Téléphone: 877 000 0000</span> </p>
        </li>
        <li>
          <h2>Horaire de notre magasin:</h2>
          <p> <span>Lundi au Vendredi de 9:00 am - 7:00 pm EST (GMT -05000)</span> <span>Nous sommes fermés le Samedi et Dimanche</span> </p>
        </li>
        <li>
          <h2>Notre Emplacement:</h2>
          <p> <span>Petshop</span> <span>250 Lorem Ipsum Street</span> <span>4th Floor</span> <span>jaonfanr, Caknan 109935</span> <span>France</span> </p>
        </li>
      </ul>
    </div>
    <div id="sidebar">
      <div class="connect">
        <h2>Follow Us</h2>
        <ul>
          <li><a class="facebook" href="#">Facebook</a></li>
          <li><a class="subscribe" href="#">Subscribe</a></li>
          <li><a class="twitter" href="#">Twitter</a></li>
          <li><a class="flicker" href="#">Flicker</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="featured">
    <ul>
      <li><a href="#"><img src="images/organic-and-chemical-free.jpg" width="300" height="90" alt=""></a></li>
      <li><a href="#"><img src="images/good-food.jpg" width="300" height="90" alt=""></a></li>
      <li class="last"><a href="#"><img src="images/pet-grooming.jpg" width="300" height="90" alt=""></a></li>
    </ul>
  </div>
</div>


<div id="footer">
  <div class="section">
    <ul>
      <li> <img src="<?php echo base_url("assets/images/friendly-pets.jpg");?>" width="240" height="186" alt="">
      </li>
      <li> <img src="<?php echo base_url("assets/images/pet-lover2.jpg");?>" width="240" height="186" alt="">
      </li>
      <li> <img src="<?php echo base_url("assets/images/healthy-dog.jpg");?>" width="240" height="186" alt="">
      </li>
      <li>
        <img src="<?php echo base_url("assets/images/pet-lover.jpg");?>" width="240" height="186" alt=""> </li>
    </ul>
  </div>
  <div id="footnote">
    <div class="section">Copyright &copy; 2012 <a href="#">Company Name</a> All rights reserved | Website Template By <a target="_blank" href="http://www.freewebsitetemplates.com/">freewebsitetemplates.com</a></div>
  </div>
</div>
</body>
</html>
