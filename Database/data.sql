/*create database petshop;*/

create sequence usr start 1;
create sequence cat start 1;
create sequence rac start 1;
create sequence anm start 1;

create table utilisateur(
  idUtilisateur varchar(7),
  nom varchar(150),
  prenoms varchar(150),
  email varchar(100),
  motdepasse varchar(200),
  permission int,
  primary key(idUtilisateur)
);
/* 1 si admin */
insert into utilisateur values('usr1','Andrianaivosoa','Tsiky','tsikyandrianaivosu@gmail.com','tsiky03',1);

create table categorie(
      idCategorie varchar(7),
      nom varchar(50),
      primary key(idCategorie)
);
insert into categorie values('cat1','Chien');
insert into categorie values('cat2','Chat');
insert into categorie values('cat3','Oiseau');
insert into categorie values('cat4','Poisson');
insert into categorie values('cat5','Hamstère');

create table race(
    idRace varchar(7),
	idCategorie varchar(7), 
    nom varchar(100),
    primary key(idRace),
	foreign key(idCategorie) references categorie(idCategorie)
);
insert into race values('rac1','cat1','Coton de tuléar');
insert into race values('rac2','cat1','Husky');
insert into race values('rac3','cat1','Berger Allemand');
insert into race values('rac4','cat2','British long Hair');

insert into race values('rac5','cat3','Pérroquet');
insert into race values('rac6','cat4','Gold Fish');
insert into race values('rac7','cat5','Pérroquet');

create table animaux(
    idAnimaux varchar(7),
    idCategorie varchar(7),
    idRace varchar(7), 
    nom varchar(100),
	poids decimal(5,2),
	taille int,	
	sexe char,
    description varchar(1000),
	naissance Date,
    prix decimal(10,2),
    titreimage varchar(100),
    referenceimage varchar(500),
    descriptionimage varchar(500),
	disponibilite int,
    primary key(idAnimaux),
    foreign key(idCategorie) references categorie(idCategorie),
    foreign key(idRace) references race(idRace)
);
insert into animaux values(concat('anm',nextval('anm')),'cat1','rac1','Cupcake',4,25,'M','Poils très doux, souple, cotonneux, dense, abondant et souvent mêlés.Couleur de fond blanche neige. Quelques traces de couleur champagne,ou mouchetée.De caractère gai, stable, très sociable avec les humains et avec ses congénères, il s’adapte parfaitement à tout style de vie. Le caractère du Coton de Tuléar est une des caractéristiques principales de la race.','2016-02-02',200,'Coton de tuléar','/images/animaux/coton_de_tuléar.jpg','Coton de tuléar',1) ;
insert into animaux values(concat('anm',nextval('anm')),'cat1','rac2','Balto',20,60,'M','Poil Double et de longueur moyenne.Tête ronde.Yeux En forme d\'amande bleus.Oreilles Triangulaires, dressées, l\'extrémité arrondie pointe vers le ciel.Queue touffue, légèrement relevée en forme de faucille.Caractère Doux et gentil, sociable et têtu','2015-02-02',1000,'Husky','/images/animaux/husky.jpg','Husky',1) ;
insert into animaux values(concat('anm',nextval('anm')),'cat1','rac3','Moka',15,50,'F','Poil Court, dense et souple.Robe Noire charbonné marquée de taches brunes.Caractère Équilibré, courageux, sûr de lui, protecteur et joueur.','2017-02-02',1000,'Berger Allemand','/image/animaux/berger_allemand.jpg','Berger Allemand',1) ;
insert into animaux values(concat('anm',nextval('anm')),'cat2','rac4','Coconut',6,50,'M','Poil Long et soyeux.Couleur gris.Tête Large et ronde.Yeux Ronds et grands en vert.Oreilles Moyennes, arrondies à l\'extrémité.Queue Assez courte et épaisse à la base.Un chat calme avec le flegme légendaire de ses origines et très attaché à son propriétaire. Très joueuse et n\'aimant pas se faire porter.','2017-05-02',1000,'British Long Hair','/images/animaux/british_long_hair.jpg','British Long Hair',1) ;



insert into animaux values(concat('anm',nextval('anm')),'cat1','rac5','Kaki',1.5,15,'M','Plumes trè','2015-02-02',1000,'Husky','../image/husky.jpg','Husky') ;

insert into animaux values(concat('anm',nextval('anm')),'cat1','rac3','Moka',15,50,'F','','2015-02-02',1000,'Husky','../image/husky.jpg','Husky') ;
insert into animaux values(concat('anm',nextval('anm')),'cat1','rac3','Moka',15,50,'F','','2015-02-02',1000,'Husky','../image/husky.jpg','Husky') ;



/*



create table souscategorie(
    idSouscategorie varchar(7),
	idCategorie varchar(7), 
    nom varchar(100),
    primary key(idSouscategorie),
	foreign key(idCategorie) references categorie(idCategorie)
);
insert into sousctegorie values('sct1','cat1','Nouritture');
insert into sousctegorie values('sct2','cat1','Accessoire');

create table produits(
	idProduits varchar(7),
    idSouscategorie varchar(7),
	idCategorie varchar(7), 
    nom varchar(100),
	prix decimal(10,2),
	titreimage varchar(100),
    referenceimage varchar(500),
    descriptionimage varchar(500),
    primary key(idRace),
	foreign key(idSouscategorie) references souscategorie(idSouscategorie),
	foreign key(idCategorie) references categorie(idCategorie)
);*/